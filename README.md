# bioawk Singularity container
### Bionformatics package bioawk<br>
BWK awk modified for biological data<br>
bioawk Version: 1.0<br>
[https://github.com/lh3/bioawk]

Singularity container based on the recipe: Singularity.bioawk_v1.0

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bioawk_v1.0.sif Singularity.bioawk_v1.0`

### Get image help
`singularity run-help ./bioawk_v1.0.sif`

#### Default runscript: STAR
#### Usage:
  `bioawk_v1.0.sif --help`<br>
    or:<br>
  `singularity exec bioawk_v1.0.sif bioawk --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bioawk_v1.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/bioawk/bioawk:latest`


